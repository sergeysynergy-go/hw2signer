package main

import (
	"fmt"
	"sort"
	"strconv"
	"sync"
)

const maxDataLen = 100

type weightedData struct {
	weight 	int
	data 	string
}

func ExecutePipeline(jobs ...job)  {
	pipe := make([]chan interface{}, len(jobs)+1)
	// инициализируем каналы не nil значением
	for k := range pipe {
		pipe[k] = make(chan interface{})
	}

	for k, jb := range jobs {
		go func(in, out chan interface{}, jb job) {
			jb(in, out)
			close(out)  // закрываем канал, чтобы дальше считать из него посредством range
		}(pipe[k], pipe[k+1], jb)
	}

	// Выводим результаты, если не считывать, конвейер завершит работу не дожидаясь выполнения
	for out := range pipe[len(pipe)-1] {
		fmt.Sprintf(out.(string))
		//fmt.Println("out::::", out)
	}
}

type md5Input struct {
	data 		string
	resCh 		chan string
}

func SingleMd5Worker(input chan interface{})  {
	for i := range input {
		md5 := DataSignerMd5(i.(md5Input).data)
		out := i.(md5Input).resCh
		out <- md5
	}
}

func getCrc32(data string) chan string {
	// надо использовать буферизированный канал
	result := make(chan string, 1)
	go func() {
		result <- DataSignerCrc32(data)
		close(result)
	}()
	return result
}

func SingerWorker(wg *sync.WaitGroup, num int, out, md5In chan interface{})  {
	defer wg.Done()
	data := strconv.Itoa(num)
	// Создаём канал с результатом, чтобы вычисления вернулись именно в нужную копию горутины
	md5Res := make(chan string)
	md5Data := md5Input{data, md5Res}

	// Запускаем асинхронное вычисление первой crc32 суммы через функцию, возвращающую канал в качестве результата
	res1 := getCrc32(data)

	// Запускаем вычисление md5 суммы через общий воркер, посредством записи данных во входной канал воркера
	md5In <- md5Data

	// Запускаем асинхронное вычисление второй crc32 суммы сразу после получения результата от общего воркера
	md5 := <-md5Res
	res2 := getCrc32(md5)

	res := <-res1+"~"+<-res2
	out <- res
}

var SingleHash = job(func(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	md5In := make(chan interface{}, maxDataLen)
	go SingleMd5Worker(md5In)
	for val := range in {
		wg.Add(1)
		go SingerWorker(wg, val.(int), out, md5In)
	}
	wg.Wait()
})

// multiHashGroupNoMap реализация без мьютекс блокировки. Группа асинхронных обработчиков подсчёта хэш-суммы crc32.
// Пишет результаты в канал типа weightedData (пронумерованные данные).
func multiHashGroupNoMap(groupsNumber int, data string, out chan weightedData) {
	wg := &sync.WaitGroup{}
	for i := 0; i < groupsNumber; i ++ {
		k := i
		wg.Add(1)
		go func() {
			defer wg.Done()
			res := weightedData{
				weight: k,
				data: DataSignerCrc32(strconv.Itoa(k)+data),
			}
			out <- res
		}()
	}
	wg.Wait()
	close(out)
}

// MultiHash реализация воркера без мьютекс блокировки
var MultiHash = job(func(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	for val := range in {
		data := val.(string)
		wg.Add(1)
		go func() {
			defer wg.Done()
			multiLen := 6
			multi := make(map[int]string, multiLen)
			resCh := make(chan weightedData, multiLen)
			go multiHashGroupNoMap(multiLen, data, resCh)
			// Сортируем и склеиваем пронумерованные данные типа weightedData перед отправкой в канал результатов.
			for v := range resCh {
				multi[v.weight] = v.data
			}
			res := ""
			for i := 0; i < multiLen; i ++ {
				res += multi[i]
			}
			out <- res
		}()
	}
	wg.Wait()
})

// CombineResults накапливаем результаты, сортируем, конкатенируем через `_`
var CombineResults = job(func(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	var buf []string
	for val := range in {
		buf = append(buf, val.(string))
	}
	sort.Strings(buf)
	res := ""
	for i := 0; i < len(buf)-1; i++ {
		res += buf[i] + "_"
	}
	res += buf[len(buf)-1]
	out <- res
	wg.Wait()
})
