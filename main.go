package main

import (
	"fmt"
	"strconv"
	"sync"
)

// MultiHashGroupMap реализация сохранения результатов конкурентного выполнения горутин в map, через мьютекс блокировку
func MultiHashGroupMap(wg *sync.WaitGroup, data string, out chan interface{}) {
	defer wg.Done()
	multiLen := 6
	type multik map[int]string
	multi := make(multik, multiLen)
	wg2 := &sync.WaitGroup{}
	mtx := sync.Mutex{}
	for i := 0; i < multiLen; i ++ {
		th := i
		wg2.Add(1)
		go func(wg *sync.WaitGroup, multi multik, i int, data string) {
			res := DataSignerCrc32(strconv.Itoa(th)+data)
			mtx.Lock()
			multi[i] = res
			mtx.Unlock()
			wg.Done()
		}(wg2, multi, i, data)
	}
	wg2.Wait()
	res := ""
	for i := 0; i < multiLen; i ++ {
		res += multi[i]
	}
	out <- res
}

// MultiHashMap реализация сохранения результатов конкурентного выполнения горутин в map, через мьютекс блокировку
var MultiHashMap = job(func(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	for val := range in {
		data := val.(string)
		wg.Add(1)
		go MultiHashGroupMap(wg, data, out)
	}
	wg.Wait()
})

func tests()  {
	//ExecutePipeline(jobs...)
	//res1 := SingleHsh(0)
	fmt.Println(":::: 4108050209~502633748")
	//fmt.Println("RES:", res1)
	//res1 := "4108050209~502633748"

	//res2 := SingleHsh(1)
	fmt.Println(":::: 2212294583~709660146")
	//fmt.Println("RES:", res2)
	//res2 := "2212294583~709660146"

	//multiRes1 := MultiHashWorker(res1)
	fmt.Println("::::::::::::::::: 29568666068035183841425683795340791879727309630931025356555")
	//fmt.Println("MultiHash result:", multiRes1)

	//multiRes2 := MultiHashWorker(res2)
	fmt.Println("::::::::::::::::: 4958044192186797981418233587017209679042592862002427381542")
	//fmt.Println("MultiHash result:", multiRes2)
}
